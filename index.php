<?php require 'action.php';?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>tz</title>
		<link rel="stylesheet" href="style.css">
    </head>
    <body>
		<div id="add">
		<h1>Заполните форму</h1>
		<form action="add.php" method="post">
			<p>ФИО:</p>
			<input type="text" name="name" placeholder="Иванов Иван Иванович">
			<p>Телефон:</p>
			<input type="text" name="phone" placeholder="+7 916 787878">
			<p>E-mail:</p>
			<input type="text" name="email" placeholder="ivan@mail.ru">
			</br>
			<input type="submit" value="Отправить">			
		</form>
		</div>
		<div id="table">
			<table>
				<tr>
					<td class="nom"><b>№</b></td>
					<td class="fio"><b>ФИО</b></td>
					<td class="phone"><b>Телефон</b></td>
					<td class="mail"><b>E-mail</b></td>
				</tr>
				<?php foreach($res as $list): ?>
				<tr>
					<td class="nom"><?=$list["id"]?></td>
					<td class="fio"><?=$list["name"]?></td>
					<td class="phone"><?=$list["phone"]?></td>
					<td class="mail"><?=$list["email"]?></td>
				</tr>
				<?php endforeach;?>
			</table>
		</div>
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="js.js"></script>
    </body>
</html>
